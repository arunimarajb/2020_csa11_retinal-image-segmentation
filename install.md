Installation Procedure

Installing Python

We are implementing our code in Python Language. We have used version 3.9.0 .
It comes inbuilt with most of the machines.
To check version of python use the command: python3 -V

Installing PIP

PIP is the package manager for python. It is included by default in Python. We
need to install pip for installing following packages.
• To install pip use the command: sudo apt-get install python-pip
• To check the version use code: pip -V

Installing NumPy

To convert our image into an array and for doing operations on it, we need NumPy.
It is a package in Python used for scientific calculation.
We install NumPy using the command: pip install numpy

Installing OpenCV

Our project is mainly based on the domain OpenCV. It is a library used for real
time applications using computer vision.
To install OpenCV use the command: pip install opencv-python

Installing Matplotlib

We need to plot the image at different stages of the project. For that, we have used
Matplotlib library. It is a comprehensive library for creating static, animated, and
interactive visualizations in Python.
We install matplotlib using the command: sudo apt-get install python3-
matplotlib

Installing Pillow

We use the package pillow to import images.Pillow is a Python Imaging Library
(PIL), which adds support for opening, manipulating, and saving images. The
current version identifies and reads a large number of formats.
To install pillow use the command: pip install Pillow

Run the code in terminal :gcc filename.py 
